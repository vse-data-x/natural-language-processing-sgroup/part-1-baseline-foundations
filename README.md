# Part-1-Natural language processing baseline foundations
If we want to build applications with NLP embedded there are different approaches, we will quickly go through some of them, and we will make emphasis on the modern ones. But I suggest you to become very familiar with the traditional ones, because sometimes they can be good enough for your use case, quick and cheap. In addition to that they can also serve as a baseline to start experimenting.

## Working Example

We will implement a NLP Pipeline and run a very traditional task  called *Text classification*. 

### Text clasification pipeline

1. Collect training data
2. Preprocess data, extract features
3. Train and evaluate algorithm 
4. Predict
